#!/usr/bin/bash
read INPUT
k=0
VARGO="$(date)"
while [[ $k -lt "2**$INPUT" ]]
do
echo >>  README.md "$(( 2**$k ))"
let $(( k=$k+1 ))
done
VARSTOP="$(date)"
echo >>  README.md "$VARGO"
echo >>  README.md "$VARSTOP"
cat README.md
