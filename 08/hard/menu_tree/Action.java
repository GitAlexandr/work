package menu;

abstract class Action{
  String message;

  public Action(String message){
    this.message = message;
  }

  public String getMessage(){
    return this.message;
  }

  abstract public void doSmth() throws Exception;

}
