package menu;
import java.util.*;

public class Node{
  public ArrayList<Node> child = new ArrayList<Node>();
  public ArrayList<Action> action = new ArrayList<Action>();
  private Node parent;

  public void setParent(Node parent){
    this.parent = parent;
  }

  public Node getParent(){
    return this.parent;
  }

  public void setAction(Action action){
    this.action.add(action);
  }

  public void setChild(Node child){
    this.child.add(child);
    child.parent = this; //чтобы создался дальше ребенок от другого ребенка
  }

  public int sizeChild(){
    return this.child.size();
  }

  public int sizeAction(){
    return this.action.size();
  }

  public void output(){
    System.out.println("добро пожаловать: ");
    if (this.parent == null){
      System.out.println("0. Выйти из меню");
    }
    else{
      System.out.println("0. Вернуться на уровень выше");
    }
    int level = 1;
    for (int i = 0; i < sizeChild(); i++){
      System.out.println(level + ". " + "перейти на следующий уровень");
      level++;
    }

    for (int i = 0; i < sizeAction(); i++){
      System.out.println(level + ". " + this.action.get(i).getMessage());
      level++;
    }
  }



}
