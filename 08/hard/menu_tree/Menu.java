package menu;

import java.util.*;
class Menu {
  public static void main(String[] args) throws Exception {
    Scanner sc = new Scanner(System.in);
    int userRequest = 0;
    Node begin = nodes();
    boolean osh = true; 

    while (osh) {
      begin.output();
      userRequest = sc.nextInt();
      System.out.println();
      if (userRequest == 0) {
        if (begin.getParent() == null) {
          osh = false;
        } else {
          begin = begin.getParent();
        }
      }
      if (userRequest > begin.sizeChild() && userRequest <= begin.sizeAction() + begin.sizeChild()) {
        begin.action.get(userRequest - begin.sizeChild() - 1).doSmth();
      }
      if (userRequest > 0 && userRequest <= begin.sizeChild()) {
        begin = begin.child.get(userRequest - 1);
      }
    }

  }

  public static Node nodes() {
    Node begin = new Node();
    Node level1 = new Node();
    Node level2 = new Node();
    begin.setChild(level1);
    begin.setAction(new Action1("Умножение"));
    level1.setAction(new Action2("Сложение"));
    level1.setChild(level2);
    level2.setAction(new Action3("Вычитание"));
    return begin;
  }
}

