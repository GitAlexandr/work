/*
Создать программу, моделирующую случайный процесс.
С клавиатуры задается некий "порог" в виде вещественного числа,
коэффициент влияния порога и максимальное время моделирования.
В случайные моменты времени генерируются случайные вещественные
числа в интервале [-коэффициент*порог; коэффициент*порог].
Если сгенерированное число больше заданного порога, то прекратить
моделирование. По завершении моделирования вывести следующую
информацию на консоль: <время от начала эксперимента>: <число>
*/

package hard;

import java.util.Scanner;
import java.util.Date;
import java.util.Random;
public class Prog4{
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("Введите порог: ");
    double limen = scanner.nextDouble(); //порог

    System.out.print("Введите коэффициент: ");
    double coefficient = scanner.nextDouble(); //коэффициент влияния порога

    System.out.print("Введите максимальное время: ");
    long maximum_time = scanner.nextLong(); //максимальное время моделирования
    Thread thread = new Thread();
    double counter = 0;
    long start = System.currentTimeMillis();
    long end = start;
    boolean flag = true;

    while (flag){
      if (counter <= limen && ((end - start) <= maximum_time)) {
        try{
          thread.sleep(new Random().nextInt(10));
          counter = Math.random()*(2*(limen*coefficient)) - (limen*coefficient);
        }
        catch (Exception e){}
        end = System.currentTimeMillis();
      }
      else{
        flag = false;
      }
      System.out.println("" + counter + " " + (end - start));
    }
  }
}
