/*
Создать программу, моделирующую процесс. С клавиатуры задается количество повторений,
размер области и сдвиг относительно начала координат. Заданное количество раз производится
генерация 4 случайных чисел
(1 - начало отрезка 1,
2 - конец отрезка 1,
3 - начало отрезка 2,
4 - конец отрезка 2). Программа должна выводить на консоль количество пересечений отрезков.
*/
package my_repo.hard;

import java.util.Scanner;
import java.util.Random;

public class Prog5{

  public static void main(String[] args){
    Scanner scanner = new Scanner(System.in);

    System.out.println("Введите количество повторений: ");
    int repetitions = scanner.nextInt();

    System.out.println("Введите размер области (конец): ");
    int square = scanner.nextInt();


    System.out.println("Введите сдвиг относительно начала координат: ");
    int shift = scanner.nextInt();

    int summa = result(repetitions, shift, square);
    System.out.println(summa); //пересечения
  }


  public static int result(int repetitions, int shift, int square){
    int summa = 0;
    for (int i = 0; i < repetitions; i++){
      int tone = shift + new Random().nextInt(square);
      int ttwo = shift + new Random().nextInt(square);
      int tthree = shift + new Random().nextInt(square);
      int tfour = shift + new Random().nextInt(square);

      if (ttwo < tone){
        int tochka = tone;
        tone = ttwo;
        ttwo = tochka;
      }

      if (tfour < tthree){
        int tochka_2 = tthree;
        tthree = tfour;
        tfour = tochka_2;
      }

      if((tone < tthree && tthree < ttwo) || (tthree < tone && tone < tfour)){
        summa++;
      }
    }
    return summa;
  }
}
