package Prog1;

public class FibonachiRecursia {
    public static long fibonachi(int n, long[] arr){
        if (arr[n - 1] != 0){
            return arr[n - 1];
        }
        if (n <= 2){
            arr[n - 1] = n - 1;
            return arr[n - 1];
        }
        long result = fibonachi(n - 1, arr) + fibonachi(n - 2, arr);
        arr[n - 1] = result;
        return result;
    }

    public static  long[] getFibonachi(int n){
        if (n == 0){
            return null;
        }
        long[] arr_1 = new long[n];
        fibonachi(n, arr_1);
        return arr_1;
    }
}