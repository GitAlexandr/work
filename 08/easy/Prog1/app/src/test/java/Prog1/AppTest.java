package Prog1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class AppTest {
  @Test void fibonachi() {
    int a = 1;
    long[] fibonachi = {0};
    assertArrayEquals(fibonachi, Fibonachi.fibo(a));
  }

  @Test void recursia() {
    int a = 11;
    long[] fibonachiRec = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55};
    assertArrayEquals(fibonachiRec, FibonachiRecursia.getFibonachi(a));
  }
}