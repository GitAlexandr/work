class sortirovka{
	public static void main(String[] args){
		int[] massive = new int[] {1, 2, 5, 8, 7};
		int mem;

		for (int i = 0; i < massive.length-1; ++i){
			if (i + 1 < massive.length && massive[i] > massive[i+1]){
				mem = massive[i];
				massive[i] = massive[i+1];
				massive[i+1] = mem;
			}
		}
		for (int i = 0; i < massive.length; ++i){
			System.out.print(massive[i]);
		}
	}
}

