#include <stdio.h>
int F(int a, int b) {
  int k = a+b;
  return k;
}
int main(void) {
  int a = 1000;
  int b = 1000000;
  int sum = F(a, b);
  printf("Сумма чисел %d", a);
  printf(" и %d", b);
  printf(" равна %d\n", sum);
  return 0;
}
